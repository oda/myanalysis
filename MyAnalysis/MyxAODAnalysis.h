#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H
 
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>
#include <MuonSelectorTools/MuonSelectionTool.h>
#include <memory>
#include <vector>
 
class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
 
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
 
 private:
  unsigned int m_runNumber = 0;
  unsigned int m_lumiBlock = 0;
  unsigned long long m_eventNumber = 0;
  std::unique_ptr<std::vector<float>> m_muonE;
  std::unique_ptr<std::vector<float>> m_muonPt;
  std::unique_ptr<std::vector<float>> m_muonEta;
  std::unique_ptr<std::vector<float>> m_muonPhi;

  ToolHandle<CP::MuonSelectionTool> m_selTool;
};
 
#endif
