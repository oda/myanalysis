#!/usr/bin/env python
 
# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()
 
# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()
 
# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath = '/gpfs/fs7001/oda/tutorial2020/data18_13TeV.00352448.physics_Main.deriv.DAOD_STDM3.f938_m1979_p4096'
ROOT.SH.ScanDir().filePattern( '*.pool.root*' ).scan( sh, inputFilePath )
sh.printContent()
 
# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 10 ) # maximum number of events to be analyzed. If you want to see all events, please set it to -1
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')
 
# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
 
# later on we'll add some configuration options for our algorithm that go here

from AnaAlgorithm.DualUseConfig import addPrivateTool
 
# add the muon selection tool to the algorithm                                                                                                                                                                    
addPrivateTool( alg, 'MuonSelTool', 'CP::MuonSelectionTool' )
alg.MuonSelTool.MuQuality = 1 # 0 Tight, 1 Medium, 2 Loose, 3 VeryLoose, 4 HighPt, 5 LowPtEfficiency
 
# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
 
# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
