#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/MuonContainer.h>
#include <TH1.h>
#include <TTree.h>
 
 
 
MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
    m_selTool ("CP::MuonSelectionTool", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  declareProperty ("MuonSelTool", m_selTool, "The muon selection tool");
}
 
 
 
StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TH1F ("h_nMuons", "Number of muons", 11, -0.5, 10.5)));
  ANA_CHECK (book (TH1F ("h_muon_e", "Muon energy [GeV]", 100, 0., 250.)));
  ANA_CHECK (book (TH1F ("h_muon_pt", "Muon p_{T} [GeV]", 100, 0., 250.)));
  ANA_CHECK (book (TH1F ("h_muon_eta", "Muon #eta", 100, -3.0, 3.0)));
  ANA_CHECK (book (TH1F ("h_muon_phi", "Muon #phi [radian]", 100, -M_PI, M_PI)));

  ANA_CHECK (book (TTree ("analysis", "My analysis tree")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("LumiBlock", &m_lumiBlock);
  mytree->Branch ("EventNumber", &m_eventNumber);
  m_muonE = std::make_unique<std::vector<float>>();
  mytree->Branch ("muonE", &*m_muonE);
  m_muonPt = std::make_unique<std::vector<float>>();
  mytree->Branch ("muonPt", &*m_muonPt);
  m_muonEta = std::make_unique<std::vector<float>>();
  mytree->Branch ("muonEta", &*m_muonEta);
  m_muonPhi = std::make_unique<std::vector<float>>();
  mytree->Branch ("muonPhi", &*m_muonPhi);

  ANA_CHECK (m_selTool.retrieve());

  return StatusCode::SUCCESS;
}
 
 
 
StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_INFO ("in execute");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
 
  // print out run and event number from retrieved object
  ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() <<
                ", lumiBlock = " << eventInfo->lumiBlock() <<
                ", eventNumber = " << eventInfo->eventNumber());

  m_runNumber = eventInfo->runNumber ();
  m_lumiBlock = eventInfo->lumiBlock ();
  m_eventNumber = eventInfo->eventNumber ();

  // Muons
  const xAOD::MuonContainer* muons = nullptr;
  ANA_CHECK(evtStore()->retrieve(muons, "Muons"));
  unsigned int nMuons = 0;

  m_muonE->clear();
  m_muonPt->clear();
  m_muonEta->clear();
  m_muonPhi->clear();

  for (const xAOD::Muon* muon : *muons) {
    if (!m_selTool->accept(*muon)) continue;

    ANA_MSG_INFO ("  muon pt = " << muon->pt()*0.001 << " GeV");
    hist ("h_muon_e")->Fill (muon->e() * 0.001); // GeV
    hist ("h_muon_pt")->Fill (muon->pt() * 0.001); // GeV
    hist ("h_muon_eta")->Fill (muon->eta());
    hist ("h_muon_phi")->Fill (muon->phi());

    m_muonE->push_back (muon->e() * 0.001); // GeV
    m_muonPt->push_back (muon->pt() * 0.001); // GeV
    m_muonEta->push_back (muon->eta());
    m_muonPhi->push_back (muon->phi());

    nMuons++;
  }
  hist ("h_nMuons")->Fill (nMuons);

  // Fill the event into the tree:
  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
}
 
 
 
StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
