Package for ATALAS Japan software tutorial in 2020 (https://wiki.kek.jp/pages/viewpage.action?pageId=127280340) based on https://atlassoftwaredocs.web.cern.ch/ABtutorial/

```
mkdir tutorial2020
cd tutorial2020
mkdir build run source
cd build
setupATLAS
lsetup git
asetup AnalysisBase 21.2.133 here 
cp CMakeLists.txt ../source
cd ../source
git clone ssh://git@gitlab.cern.ch:7999/oda/MyAnalysis.git
cd ../build
cmake ../source
make
source x86_64-centos7-gcc8-opt/setup.sh
cd ../run
# edit inputFilePath of ../source/MyAnalysis/share/ATestRun_eljob.py
ATestRun_eljob.py --submission-dir=submitDir1 2>&1 | tee test1.log
```
